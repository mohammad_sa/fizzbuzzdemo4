﻿$(document).ready(function () {
    var index = document.getElementsByClassName("pageIndex");
    for (var countIndex = 0; countIndex < index.length; countIndex++) {
        index[countIndex].setAttribute("href", "javascript:PagerClick(" + index[countIndex].innerText + ")")
    }
});

function PagerClick(index) {
    document.getElementById("hfCurrentPageIndex").value = index;
    document.forms[0].submit();
}