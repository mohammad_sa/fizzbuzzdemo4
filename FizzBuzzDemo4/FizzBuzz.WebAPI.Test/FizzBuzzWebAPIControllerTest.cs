using System;
using System.Collections.Generic;
using Xunit;
using FizzBuzz.WebAPIService.Controllers;
using FizzBuzz.WebAPIService;
using FizzBuzz.WebAPIService.CreateText;
using FizzBuzz.WebAPIService.BusinessLogic.DayOfTheWeek;
using FizzBuzz.WebAPIService.BusinessLogic;
using FizzBuzz.WebAPIService.BusinessLogic.Rule;

namespace FizzBuzz.WebAPI.Test
{
    public class FizzBuzzWebAPIControllerTest
    {
        [Theory, MemberData(nameof(InputAndExpectedData))]
        public void Test_GenerateFizzBuzzMethod(int inputValue1, List<string> expectedValue)
        {
            //Arrange
            ITextService messages = new TextService(new FizzRule());
            var result = new FizzBuzzWebAPIController(messages);

            //Act
            IList<string> actualResult = result.GetMessages(inputValue1);

            //Assert
            Equals(expectedValue, actualResult);
        }

        public static IEnumerable<object[]> InputAndExpectedData =>
           new List<object[]>
           {
                new object[] { 15,  new List<string> { "1", "2", "Fizz", "4", "Buzz", "6", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" } }
           };
    }
}
