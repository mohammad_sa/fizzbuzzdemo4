﻿using FizzBuzz.WebAPIService.BusinessLogic;
using FizzBuzz.WebAPIService.BusinessLogic.DayOfTheWeek;
using FizzBuzz.WebAPIService.BusinessLogic.Rule;
using FizzBuzz.WebAPIService.CreateText;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.WebAPI.Test
{
    public class CreateTextServiceTest
    {
        private readonly IEnumerable<IRule> rules;
        public CreateTextServiceTest()
        {
            rules = new List<IRule>
            {
                new FizzRule(new DayOfTheWeek()),
                new BuzzRule(new DayOfTheWeek()),
                new FizzBuzzRule(new DayOfTheWeek())
            };
        }
        [Theory, MemberData(nameof(InputAndExpectedData))]
        public void Test_GenerateFizzBuzzMethod(int inputValue1, List<string> expectedValue)
        {
            //Arrange            
            var result = new TextService(rules);

            //Act
            IList<string> actualResult = result.GetMessages(inputValue1);

            //Assert
            Equals(expectedValue, actualResult);
        }

        public static IEnumerable<object[]> InputAndExpectedData =>
           new List<object[]>
           {
                new object[] { 5, new List<string> { "1", "2", "Wizz", "4", "Wuzz" } },
                new object[] { 5, new List<string> { "1", "2", "Fizz", "4", "Buzz" } },
                new object[] { 15, new List<string> { "1", "2", "Fizz", "4", "Buzz", "6", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" } }
           };

        [Theory]
        [InlineData(30, "Wizz Wuzz")]
        [InlineData(33, "Wizz")]
        [InlineData(35, "Wuzz")]
        [InlineData(30, "Fizz Buzz")]
        [InlineData(33, "Fizz")]
        [InlineData(35, "Buzz")]
        [InlineData(29, "29")]
        public void Test_ProcessFizzBuzzDataMethod(int inputValue1, string expectedValue)
        {
            //Arrange
            var result = new TextService(rules);

            //Act
            var actualResult = result.ProcessFizzBuzzData(inputValue1);

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }
    }
}
