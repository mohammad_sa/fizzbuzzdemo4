﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzz.WebAPIService.BusinessLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FizzBuzz.WebAPIService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FizzBuzzWebAPIController : ControllerBase
    {
        private readonly ITextService _TextService;

        public FizzBuzzWebAPIController(ITextService textService)
        {
            _TextService = textService;
        }

        [HttpGet("{inputNumber}")]
        public IList<string> GetMessages(int inputNumber)
        {
            return _TextService.GetMessages(inputNumber);
        }
    }
}
