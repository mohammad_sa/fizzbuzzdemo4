﻿using FizzBuzz.WebAPIService.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.CreateText
{
    public class TextService : ITextService
    {
        private readonly IEnumerable<IRule> _Rules;

        public TextService(IEnumerable<IRule> Rules)
        {
            _Rules = Rules;
        }
        public IList<string> GetMessages(int inputNumber)
        {
            IList<string> messages = new List<string>();

            for (int sequenceNumber = 1; sequenceNumber <= inputNumber; sequenceNumber++)
            {
                messages.Add(ProcessFizzBuzzData(sequenceNumber));
            }
            return messages;
        }

        public string ProcessFizzBuzzData(int sequenceNumber)
        {
            foreach (var rule in _Rules)
            {
                if (rule.IsNumberMatched(sequenceNumber))
                {
                    return rule.GetReplacedWord();
                }
            }
            return Convert.ToString(sequenceNumber);
        }
    }
}
