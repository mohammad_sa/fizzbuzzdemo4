﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.BusinessLogic
{
    public interface IDayOfTheWeek
    {
        DayOfWeek GetCurrentDay();
    }
}
