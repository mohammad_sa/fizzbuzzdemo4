﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService
{
    public interface ITextService
    {
        public IList<string> GetMessages(int inputNumber);

    }
}
