﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebAPIService.BusinessLogic.DayOfTheWeek
{
    public class DayOfTheWeek : IDayOfTheWeek
    {
        public DayOfWeek GetCurrentDay()
        {
            return DateTime.Today.DayOfWeek;
        }
    }
}
